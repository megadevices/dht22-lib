#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <dht22.h>
#include "hardware.h"

uint8_t DHT22Data[5];
uint8_t bitIndex = 0;
uint8_t bitCounter = 7;
uint8_t byteCounter = 0;

static GPIO_InitTypeDef PORT;

void DHT22_Init(void) {
	RCC_APB2PeriphClockCmd(DHT22_GPIO_CLOCK,ENABLE);
	PORT.GPIO_Mode = GPIO_Mode_Out_PP;
	PORT.GPIO_Pin = DHT22_GPIO_PIN;
	PORT.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(DHT22_GPIO_PORT,&PORT);
	
	//set idle mode
	DHT22_SetBit;
}

uint8_t DHT22_ReadData(void) {
	uint8_t wait;
	uint8_t i;
	
	bitIndex = 0;
  bitCounter = 7;
  byteCounter = 0;
	
	// Clear data array
	for(i=0;i<5;i++)DHT22Data[i]=0;

	// Configure port as output
	PORT.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(DHT22_GPIO_PORT,&PORT);

	// Generate start impulse for sensor
	DHT22_ResetBit;
	delay_ms(1); // Host start signal at least 800us
	DHT22_SetBit;
	
	// Switch pin to input with Pull-Up
	PORT.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(DHT22_GPIO_PORT,&PORT);

	//Check Tgo - Bus master released time (min = 20us, typ = 30us, max=200us);
	//wait up to 300 us (60x5us), if wait > 275 us, no response
	wait = 0;
	while (DHT22_Bit_1 && (wait++ < 60)) delay_us(5);
	if (wait > 55) return DHT22_RCV_NO_RESPONSE;
		
	//Check Trel - Responce to low time (min = 75us, typ = 80us, max=85us);
	//wait up to 100 us (20x5us), if wait < 70us or > 90us, bad ack1
	wait = 0;
	while (DHT22_Bit_0 && (wait++ < 20)) delay_us(5);
	if ((wait < 14) || (wait > 18)) return DHT22_RCV_BAD_ACK1;

	//Check Treh - Responce to high time (min = 75us, typ = 80us, max=85us);
	//wait up to 100 us (20x5us), if wait < 70us or > 90us, bad ack1
	wait = 0;
	while (DHT22_Bit_1 && (wait++ < 20)) delay_us(5);
	if ((wait < 14) || (wait > 18)) return DHT22_RCV_BAD_ACK2;


	//Receive 40 bits of data
	for(i=0;i<40;i++)
	{
		//skip Tlow=50us
		wait = 0;
		while (DHT22_Bit_0 && (wait++ < 20)) delay_us(5);
		if (wait > 14)
		{
			return DHT22_RCV_RCV_TIMEOUT;	
		}

		//Check TH1 - Signal "1" high time (min = 68us, typ = 70us, max=75us);
		//wait up to 100 us (20x5us), if wait < 20us or > 80us, error
		wait = 0;
		while (DHT22_Bit_1 && (wait++ < 20)) delay_us(5);
		if ((wait < 4) || (wait > 16))
		{
			return DHT22_RCV_RCV_TIMEOUT;	
		}
		
		//if pulse more 60us, "1" received
		if (wait>12)
		{
			DHT22Data[byteCounter] |= (1<<bitCounter);
		}
		if (bitCounter == 0)
		{
			//check next byte
			bitCounter = 7;
			byteCounter++;
		}
		else
		{
			bitCounter--;
		}
	}
	
	//check CRC
	byteCounter = 0;
	for(i=0;i<4;i++)
	{
		byteCounter+=DHT22Data[i];
		//for debug
		//printf("D[%d]=%02X\r\n",i,DHT22Data[i]);
	}
	//for debug
	//printf("CRC = %02X, D[5]=%02X\r\n",byteCounter,DHT22Data[4]);
	if (byteCounter!=DHT22Data[4]) return DHT22_RCV_BAD_CRC;
		return DHT22_RCV_OK;
}
	

float DHT22getTemperature(void){
  float retVal;
  retVal = DHT22Data[2] & 0x7F;
  retVal *= 256;
  retVal += DHT22Data[3];
  retVal /= 10;
  if (DHT22Data[2] & 0x80)
    retVal *= -1;
  return retVal;
}


float DHT22getHumidity(void){
  float retVal;
  retVal = DHT22Data[0];
  retVal *= 256;
  retVal += DHT22Data[1];
  retVal /= 10;
  return retVal;
}