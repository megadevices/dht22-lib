#include "stm32f10x.h"                  // Device header
#include "stdbool.h" // ��� ������� ����������

#include "hardware.h"
#include "dht22.h"

#include "string.h"
#include "stdlib.h"
/*FreeRTOS includes*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

//Real Time Clock library
#include "rtc.h"

#include "stdio.h"

#include "one_wire.h"
#include "ds18b20.h"
#include "dht22.h"




//************************************��������������� Printf***************
int sendchar(int ch);
struct __FILE {int handle;};
FILE __stdout;


int fputc(int ch, FILE *f) {
return (sendchar(ch));
}

int sendchar(int ch)
{
while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
USART_SendData(USART1, ch);
return 0;
}																																					 
//***************************************************************************


bool diods[27] = {false};
char tmp[128];
xQueueHandle xQueue;

xTaskHandle Mode_1H;
xTaskHandle Mode_2H;
xTaskHandle Mode_3H;
xTaskHandle CommandsH;
xTaskHandle CalcH;
char CalcArg[128];
float Mode1_Speed = 1000;
float Mode2_Speed = 250;
float Mode3_Speed = 10;
uint32_t response;
uint16_t humidity,temperature;

void Light(int x,int y,int z,bool isLight)
{
	int mult = 0;
	switch(y)
	{
		case 1:	mult=0; break;
		case 2: mult=9; break;
		case 3: mult=18; break;
	}
	switch(x)
	{
				case 1:
					switch(z)
					{
						case 1:
							if(isLight)
								setPinByNumber(0+mult);
							else
								resetPinByNumber(0+mult);
						break;
							
						case 2:
							if(isLight)
								setPinByNumber(1+mult);
							else
								resetPinByNumber(1+mult);
						break;
							
						case 3:
							
						if(isLight)
								setPinByNumber(2+mult);
							else
								resetPinByNumber(2+mult);
						break;
					}
					break;
				 case 2:
					 switch(z)
					{
						case 1:
							if(isLight)
								setPinByNumber(3+mult);
							else
								resetPinByNumber(3+mult);
						break;
							
						case 2:
							if(isLight)
								setPinByNumber(4+mult);
							else
								resetPinByNumber(4+mult);
						break;
							
						case 3:
							
						if(isLight)
								setPinByNumber(5+mult);
							else
								resetPinByNumber(5+mult);
						break;
					}
					break;
				case 3:
					 switch(z)
					{
						case 1:
							if(isLight)
								setPinByNumber(6+mult);
							else
								resetPinByNumber(6+mult);
						break;
							
						case 2:
							if(isLight)
								setPinByNumber(7+mult);
							else
								resetPinByNumber(7+mult);
						break;
							
						case 3:
							
						if(isLight)
								setPinByNumber(8+mult);
							else
								resetPinByNumber(8+mult);
						break;
					}
					break;
							
	 }
}


void Mode_1(void *pvParameters)
{
	
	while(1)
	{
		Light(2,3,2,1);
		vTaskDelay(Mode1_Speed/portTICK_RATE_MS);
		Light(2,3,2,0);
		
		
		
		
		Light(2,2,2,1);
		vTaskDelay(Mode1_Speed/portTICK_RATE_MS);
		Light(2,2,2,0);	
		
		Light(2,1,2,1);
		vTaskDelay(Mode1_Speed/portTICK_RATE_MS);
		Light(2,1,2,0);
		
		
		
		Light(1,1,1,1);
		Light(1,1,2,1);
		Light(1,1,3,1);
		Light(2,1,1,1);
		Light(2,1,3,1);
		Light(3,1,1,1);
		Light(3,1,2,1);
		Light(3,1,3,1);
		vTaskDelay(Mode1_Speed/portTICK_RATE_MS);
		Light(1,1,1,0);
		Light(1,1,2,0);
		Light(1,1,3,0);
		Light(2,1,1,0);
		Light(2,1,3,0);
		Light(3,1,1,0);
		Light(3,1,2,0);
		Light(3,1,3,0);
		
		Light(1,2,1,1);
		Light(1,2,2,1);
		Light(1,2,3,1);
		Light(2,2,1,1);
		Light(2,2,3,1);
		Light(3,2,1,1);
		Light(3,2,2,1);
		Light(3,2,3,1);
		vTaskDelay(Mode1_Speed/portTICK_RATE_MS);
		Light(1,2,1,0);
		Light(1,2,2,0);
		Light(1,2,3,0);
		Light(2,2,1,0);
		Light(2,2,3,0);
		Light(3,2,1,0);
		Light(3,2,2,0);
		Light(3,2,3,0);
		
		
		Light(1,3,1,1);
		Light(1,3,2,1);
		Light(1,3,3,1);
		Light(2,3,1,1);
		Light(2,3,3,1);
		Light(3,3,1,1);
		Light(3,3,2,1);
		Light(3,3,3,1);
		vTaskDelay(Mode1_Speed/portTICK_RATE_MS);
		Light(1,3,1,0);
		Light(1,3,2,0);
		Light(1,3,3,0);
		Light(2,3,1,0);
		Light(2,3,3,0);
		Light(3,3,1,0);
		Light(3,3,2,0);
		Light(3,3,3,0);
		
	}
}



void Mode_2(void *pvParameters)
{
	while(1)
	{
		Light(1,3,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,3,3,0);
		
		Light(1,3,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,3,2,0);
		
		Light(1,3,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,3,1,0);
		
	
		
		Light(1,2,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,2,1,0);
		
		Light(1,2,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,2,2,0);
		
		Light(1,2,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,2,3,0);
		
		
		Light(1,1,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,1,3,0);
		
		Light(1,1,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,1,2,0);
		
		Light(1,1,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,1,1,0);
		
		
		Light(2,1,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,1,1,0);
		
		Light(2,1,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,1,2,0);
		
		Light(2,1,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,1,3,0);
		
		Light(2,2,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,2,3,0);
		
		Light(2,2,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,2,2,0);
		
		Light(2,2,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,2,1,0);
		
		Light(2,3,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,3,1,0);
		
		Light(2,3,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,3,2,0);
		
		Light(2,3,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,3,3,0);
		
		Light(3,3,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,3,3,0);
		
		Light(3,3,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,3,2,0);
		
		Light(3,3,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,3,1,0);
		
	
		
		Light(3,2,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,2,1,0);
		
		Light(3,2,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,2,2,0);
		
		Light(3,2,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,2,3,0);
		
		
		Light(3,1,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,1,3,0);
		
		Light(3,1,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,1,2,0);
		
		Light(3,1,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(3,1,1,0);
		
		Light(2,1,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,1,1,0);
		
		Light(2,1,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,1,2,0);
		
		Light(2,1,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,1,3,0);
		
		Light(2,2,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,2,3,0);
		
		Light(2,2,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,2,2,0);
		
		Light(2,2,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,2,1,0);
		
		Light(2,3,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,3,1,0);
		
		Light(2,3,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,3,2,0);
		
		Light(2,3,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(2,3,3,0);
		
		/*
		Light(1,3,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,3,3,0);
		
		Light(1,3,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,3,2,0);
		
		Light(1,3,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,3,1,0);
		
		Light(1,2,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,2,1,0);
		
		Light(1,2,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,2,2,0);
		
		Light(1,2,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,2,3,0);
		
		Light(1,1,3,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,1,3,0);
		
		Light(1,1,2,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,1,2,0);
		
		Light(1,1,1,1);
		vTaskDelay(Mode2_Speed/portTICK_RATE_MS);
		Light(1,1,1,0);
		*/
	}
}

void Mode_3(void *pvParameters)
{
	int i =0;
	while(1)
	{
		for(i = 0;i<27;i++)
		{			
			if(!diods[i])
			resetPinByNumber(i);
			else
			{
				setPinByNumber(i);
				if(Mode3_Speed != 0)
				vTaskDelay(Mode3_Speed / portTICK_RATE_MS);
			}
		}
	}
}
void Calc(void *pvParameters)//������� ������������
{
	
	
	//5+3*12-10/2
	//5+36-10/2
	//5+36-5
	//41-5
	//36
	while(1)
	{
		typedef struct
		{
			float num;
			char sign;
			
		}NUM;
		
		
		
		NUM nums[20];
		int i;
		NUM temp;
		int counter = 0;
		int numc = 0;
		char previous = '+';
		char buf[20] = "";
		float ans = 0;
		
		printf("%s",CalcArg);
		while (1)
		{
			switch (CalcArg[counter])
			{
			case '\0':
				nums[numc].sign = previous;
				strncpy(buf, CalcArg, counter);
				for (i = 0; i < counter; i++)
				{
					CalcArg[i] = ' ';
				}

				nums[numc].num = atof(buf);
				previous = ' ';
				numc++;
				break;
			case '+':
				nums[numc].sign = previous;
				strncpy(buf, CalcArg, counter);
				for (i = 0; i < counter+1; i++)
				{
					CalcArg[i] = ' ';
				}

				nums[numc].num = atof(buf);
				previous = '+';
				numc++;
				break;

			case '-':
				nums[numc].sign = previous;
				strncpy(buf, CalcArg, counter);
				for (i = 0; i < counter; i++)
				{
					CalcArg[i] = ' ';
				}

				nums[numc].num = atof(buf);
				previous = '+';
				numc++;
				break;
			
			case '*':
				nums[numc].sign = previous;
				strncpy(buf, CalcArg, counter);
				for (i = 0; i < counter+1; i++)
				{
					CalcArg[i] = ' ';
				}
				
				nums[numc].num = atof(buf);
				previous = '*';
				numc++;
				break;
			case '/':
				nums[numc].sign = previous;
				strncpy(buf, CalcArg, counter);
				for (i = 0; i < counter+1; i++)
				{
					CalcArg[i] = ' ';
				}

				nums[numc].num = atof(buf);
				previous = '/';
				numc++;
				break;
			default:
				break;
			}


			
			if (CalcArg[counter] == '\0') break;
			counter++;
			
			

		}
		for (i = 0; i < numc; i++)
		{
			if (nums[i].sign == '*')
			{
				temp.num = nums[i - 1].num *nums[i].num;
				nums[i - 1].num = 0;
				nums[i - 1].sign = ' ';
				nums[i].num = temp.num;

				if (temp.num >= 0)
					nums[i].sign = '+';
				else
					nums[i].sign = '-';
			}
			if (nums[i].sign == '/')
			{
				temp.num = nums[i - 1].num /nums[i].num;
				nums[i - 1].num = 0;
				nums[i - 1].sign = ' ';
				nums[i].num = temp.num;

				if (temp.num >= 0)
					nums[i].sign = '+';
				else
					nums[i].sign = '-';
			}
		}
		for (i = 0; i < numc; i++)
		{
			ans += nums[i].num;
		}
		printf(" = %.2f\r",ans);
		vTaskSuspend(NULL);
	}
}



void USART1_IRQHandler(void) //���������� ����������
{
		unsigned int index = 0;
		unsigned char temp = NULL;
		if(USART_GetFlagStatus(USART1,USART_FLAG_RXNE) == SET)
		{
			while(index < 127)
			{
				temp = getch(USART1);
				if(temp !='\r')
				{
					tmp[index]=temp;
				}
				else break;
				index++;
			}
			vTaskResume(CommandsH);
		}
}


void Commands(void* pvParameters) //��������� �������
{
	char *args[16];
	int counter = 0;
	char *ch;
	bool commandDone = false;
	bool ModeCMD = false;
	bool DiodeCMD = false;
	bool CalcCMD = false;
	bool TimeCMD = false;
	bool SensorCMD = false;

	while(1)
	{
		commandDone = false;	
		if(strlen(tmp)>0)
		{
			
			ch = strtok(tmp," ");
			while(ch!=NULL)
			{
			
				if(strcmp(ch,"Diode") == 0 || DiodeCMD)//�������� ���������� ������� �� �����������
				{
					ch = strtok(NULL," ");
					args[counter] = ch;
					DiodeCMD = true;
				}
				
				else if(strcmp(ch,"Mode") == 0 || ModeCMD)//������� ������������ ������ ������
				{
					ch = strtok(NULL," ");
					args[counter] = ch;
					ModeCMD = true;
				}
				
				else if(strcmp(ch,"Calc") == 0 || CalcCMD)//������� ������������
				{
					ch = strtok(NULL," ");
					args[counter] = ch;
					CalcCMD = true;
				}
				else if(strcmp(ch,"Time") == 0 || TimeCMD)//������� �����
				{
					ch = strtok(NULL," ");
					args[counter] = ch;
					TimeCMD = true;
				}
				else if(strcmp(ch,"Sensor") == 0 || SensorCMD)//������� �������
				{
					ch = strtok(NULL," ");
					args[counter] = ch;
					SensorCMD = true;
				}
				else
				{
					ch = strtok(NULL," ");
				}
				counter++;
			}
			
			if(DiodeCMD)//���������� ������� ���������� �������
			{
				if(counter - 1 == 1)
				{
					
					
					if(strcmp(args[0], "alloff") == 0)
					{
						int i = 0;
						for(i = 0 ;i < 27;i++)
						diods[i] = false;
						printf("All Diods is off \r");
					}
					else if(strcmp(args[0], "allon") == 0)
					{
						int i = 0;
						for(i = 0 ;i < 27;i++)
						diods[i] = true;
						printf("All Diods is on \r");
					}
					else
					{					
						if(diods[atoi(args[0])])
						{
							
							diods[atoi(args[0])] = false;
							printf("Diode %s is off\r",args[0]);
						}
						else
						{
							diods[atoi(args[0])] = true;
							printf("Diode %s is on\r",args[0]);
						}
					}
					
				}
				else 
					printf("Invalid arguments\r");
				
				DiodeCMD = false;
				commandDone = true;
			}
			if(ModeCMD)//���������� ������� ���������� ��������
			{
				if(counter - 1 == 2)
				{
						//�������� ��� �����
						int i = 0;
						for(i = 0 ;i < 27;i++)
						resetPinByNumber(i);
						//���������������� ��� ������
						vTaskSuspend(Mode_1H);
						vTaskSuspend(Mode_2H);
						vTaskSuspend(Mode_3H);
						
					
					
						if(strcmp(args[0],"1") == 0)
						{
							vTaskResume(Mode_1H);
							Mode1_Speed = atof(args[1]);
							printf("Mode 1 is Enabled with %s speed\r",args[1]);
						}
						if(strcmp(args[0],"2") == 0)
						{
							vTaskResume(Mode_2H);
							Mode2_Speed = atof(args[1]);
							printf("Mode 2 is Enabled with %s speed\r",args[1]);
						}
						if(strcmp(args[0],"3") == 0)
						{
							vTaskResume(Mode_3H);
							Mode3_Speed = atof(args[1]);
							printf("Mode 3 is Enabled with %s speed\r",args[1]);
						}
						
				}
				else 
					printf("Invalid arguments\r");
				
				ModeCMD = false;
				commandDone = true;
			}
			
			if(CalcCMD)//���������� ������� ������������
			{
				if(counter - 1 == 1)
				{
							memset(CalcArg,0,sizeof(CalcArg));
							strcpy(CalcArg,args[0]);
							vTaskResume(CalcH);
				}
				else 
					printf("Invalid arguments\r");
				
				CalcCMD = false;
				commandDone = true;
			}
		
			if(TimeCMD)//���������� ������� �����
			{
				if(counter - 1 == 2 || strcmp(args[0],"get") == 0) //Time get
				{
					static RTCTIME rtc;
					if( strcmp(args[1],"time")==0 ) // Time get time
					{
						RTC_GetTime(1,&rtc);
						printf("Current time is %02d:%02d:%02d\r\n",rtc.hour,rtc.min,rtc.sec);
					}
		
					if( strcmp(args[1],"date")==0 ) // Time get date
					{
						RTC_GetTime(1,&rtc);
						printf("Current date is %02d.%02d.%02d\r\n",rtc.mday,rtc.month,rtc.year);
					}
			}
				
			else if(counter - 1 == 5 || strcmp(args[0],"set") == 0) //Time set
				{
					static RTCTIME rtc;
					if( strcmp(args[1],"time")==0 ) // Time set time
					{
						RTC_GetTime(1,&rtc);
						rtc.hour = atoi(args[2]);
						rtc.min = atoi(args[3]);
						rtc.sec = atoi(args[4]);
						RTC_SetTime(&rtc);
						printf("Time set to %02d:%02d:%02d\r\n",rtc.hour,rtc.min,rtc.sec);
					}
		
					if( strcmp(args[1],"date")==0 ) // Time set date
					{
						RTC_GetTime(1,&rtc);
						rtc.mday = atoi(args[2]);
						rtc.month = atoi(args[3]);
						rtc.year = atoi(args[4]);
						RTC_SetTime(&rtc);			
						printf("Date set to %02d:%02d:%02d\r\n",rtc.mday,rtc.month,rtc.year);
					}
			}
			
				else
				{					
					printf("Invalid arguments\r");
					printf("get - �������� ����� ��� ���� (time/date)\r");
					printf("set - ���������� ����� ��� ����(time/date)\r");
					printf("������� �������:\r");
					printf("Time get time\r");
					printf("Time set time 3 12 30\r");
					printf("Time set date 1 5 2008\r");
				}		
				
				TimeCMD = false;
				commandDone = true;
			}
		
			if(SensorCMD)//���������� ������� �������
			{
				if(counter - 1 == 1)
				{
							if(strcmp(args[0],"temp") == 0)
							{
								//printf("Current temperature = %d\r", DHT22_GetTemperature());
							}
							else if(strcmp(args[0],"humidity") == 0)
							{
								//printf("Current humidity = %d\r", DHT22_GetHumidity());
							}
				}
				else
				{					
					printf("Invalid arguments\r");
					printf("Usage:\r");
					printf("Sensor temp\r");
					printf("Sensor humidity\r");
				}
				SensorCMD = false;
				commandDone = true;
			}
			
			if(!commandDone)//���� ���� ������� �� ������������ �������
			{
	
				printf("\"%s\"  - Unknown command\r\n",tmp);
				
				commandDone = true;
			}
			counter = 0;
			memset(tmp,0,sizeof(tmp));		
		}
		vTaskSuspend(NULL);
	}
}



void Test(void* pvParameters)
{
	while(1)
	{
			printf("ANOTHER ONE \r");
			vTaskDelay(100 / portTICK_RATE_MS);
	}
}

int main()
{
	
	SystemInit();
	USART1_Init();
	GPIO_Config();
	RTC_Init();
	delay_init();
	//ds18b20_init(GPIOC, GPIO_Pin_7, TIM2);
	DHT22_Init();
	
	//GPIO_SetBits(GPIOA,GPIO_Pin_2);		
	
	printf("Start!\r");

		//ds18b20_init(GPIOC, GPIO_Pin_6, TIM2);
    
	
		while(1)
    {
    	//ds18b20_read_temperature_all();
    	//ds18b20_wait_for_conversion();
    	//printf("%d---\r\n", ds18b20_get_precission());
    	//ds18b20_convert_temperature_all();
			//printf("Get DHT22 data \r\n");
			//DHT22_Read();
			//printf("Processing DHT22 data \r\n");
			//printf("Temperature:\t%.1f C\r\nHumidity:\t%.1f %%\r\n\r\n", DHT22getTemperature(), DHT22getHumidity());
			
			response = DHT22_ReadData();
			if (response != DHT22_RCV_OK) 
			{
				printf("DHT22_GetReadings() error = %d\r",response);
			} 
			else 
			{
				printf("Temperature:\t%.1f C\r\nHumidity:\t%.1f %%\r\n\r\n", DHT22getTemperature(), DHT22getHumidity());
			}
				
				
				
				
//				response = DHT22_DecodeReadings();
//				printf("Parity: Received = %d Actual = %d\r",response & 0xff,response >> 8);
//				if ((response & 0xff) != (response >> 8)) 
//				{
//					printf("Wrong data received.\n");
//				} 
//				else 
//				{
//					temperature = DHT22_GetTemperature();
//					humidity = DHT22_GetHumidity();

//					printf("Humidity: %d.%d \r",humidity / 10,humidity % 10);
//					printf("Temperature: ");
//					if ((temperature & 0x8000) != 0) printf("-");
//					printf("%d.%d C\r",(temperature & 0x7fff) / 10,(temperature & 0x7fff) % 10); 
//				}
			
    //}
		delay_ms(2000);
	}
	
	

/*	
	while(1)
	{
		printf("*\r");
		delay_ms(100);
	}

*/

/*
		while (1) 
		{
			DHT22_Init();
			response = DHT22_GetReadings();
			if (response != DHT22_RCV_OK) 
			{
				printf("DHT22_GetReadings() error = %d\r",response);
			} 
			else 
			{
				response = DHT22_DecodeReadings();
				printf("Parity: Received = %d Actual = %d\r",response & 0xff,response >> 8);
				if ((response & 0xff) != (response >> 8)) 
				{
					printf("Wrong data received.\n");
				} 
				else 
				{
					temperature = DHT22_GetTemperature();
					humidity = DHT22_GetHumidity();

					printf("Humidity: %d.%d \r",humidity / 10,humidity % 10);
					printf("Temperature: ");
					if ((temperature & 0x8000) != 0) printf("-");
					printf("%d.%d C\r",(temperature & 0x7fff) / 10,(temperature & 0x7fff) % 10); 
				}
			}

			vTaskDelay(1000 / portTICK_RATE_MS);
			vTaskSuspend(NULL);
		}

*/


		xQueue = xQueueCreate( 128, sizeof( char ) );
	xTaskCreate(Mode_1,"Task 1",100,NULL,1,&Mode_1H);
	xTaskCreate(Mode_2,"Task 2",100,NULL,1,&Mode_2H);
	xTaskCreate(Mode_3,"Task 3",200,NULL,1,&Mode_3H);
	xTaskCreate(Calc,"Calculator",200,NULL,1,&CalcH);
	xTaskCreate(Commands,"Commands",200,NULL,3,&CommandsH);
	xTaskCreate(Test,"Test",100,NULL,1,NULL);
	
	//vTaskSuspend(Mode_1H);
	vTaskSuspend(Mode_2H);
	vTaskSuspend(Mode_3H);
	vTaskSuspend(CalcH);
	vTaskStartScheduler();
	
	printf("Ups!");
	return 0;
}
