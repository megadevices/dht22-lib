
#define SYS_CLK SystemCoreClock    /* in this example we use SPEED_HIGH = 24 MHz */
#define DELAY_TIM_FREQUENCY 1000000 /* = 1MHZ -> timer runs in microseconds */

#include "stm32f10x.h"                  // Device header  

#include "stdbool.h"
#include "hardware.h"
#include "string.h"
#include "stdio.h"
/*FreeRTOS includes*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define USART1_BAUDRATE 115200
/*FreeRTOS variables*/
#define	ERROR_ACTION(CODE,POS)		do{}while(0)
	
//link for USART1 queue
extern xQueueHandle Commands;
	




void GPIO_Config()
{
 GPIO_InitTypeDef GPIO_InitStructure_A;
	GPIO_InitTypeDef GPIO_InitStructure_B;
 GPIO_InitTypeDef GPIO_InitStructure_C;
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
 
 GPIO_InitStructure_A.GPIO_Speed = GPIO_Speed_50MHz;
 GPIO_InitStructure_A.GPIO_Mode  = GPIO_Mode_Out_PP;
 GPIO_InitStructure_A.GPIO_Pin   = GPIO_Pin_2 | GPIO_Pin_3;
	
 GPIO_InitStructure_B.GPIO_Speed = GPIO_Speed_50MHz;
 GPIO_InitStructure_B.GPIO_Mode  = GPIO_Mode_Out_PP;
 GPIO_InitStructure_B.GPIO_Pin   = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_10 | GPIO_Pin_12 | GPIO_Pin_14 | GPIO_Pin_13 | GPIO_Pin_15;
	
 GPIO_InitStructure_C.GPIO_Speed = GPIO_Speed_50MHz;
 GPIO_InitStructure_C.GPIO_Mode  = GPIO_Mode_Out_PP;
 GPIO_InitStructure_C.GPIO_Pin   = GPIO_Pin_9 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13;
	
	
 GPIO_Init(GPIOA, &GPIO_InitStructure_A);
 GPIO_Init(GPIOB, &GPIO_InitStructure_B);
 GPIO_Init(GPIOC, &GPIO_InitStructure_C);
}	
	
unsigned char getch(USART_TypeDef* USARTx)
{
	unsigned char tmp;
	while(USART_GetFlagStatus(USARTx,USART_FLAG_RXNE) == RESET);
	tmp = USART_ReceiveData(USARTx);
	return tmp;
}

bool Read(USART_TypeDef* USARTx,char* s)
{
		unsigned int index = 0;
		unsigned char temp = NULL;
		if(USART_GetFlagStatus(USARTx,USART_FLAG_RXNE) == RESET)
			return false;
		
		while(index < 128)
		{
			temp = getch(USARTx);
			if(temp !='\r')
			{
				*s++=temp;	
			}
			else break;
			index++;
		}
			return true;
}


bool getPortState(GPIO_TypeDef *port,int pin)
{
	return ((port->ODR & pin) != 0);
}
void resetPinByNumber(int pin)
{
	/*
	
								GPIO_SetBits(GPIOB,GPIO_Pin_5);
								GPIO_SetBits(GPIOB,GPIO_Pin_6);
								GPIO_SetBits(GPIOB,GPIO_Pin_7);
								GPIO_SetBits(GPIOC,GPIO_Pin_11);
								GPIO_SetBits(GPIOC,GPIO_Pin_12);
								GPIO_SetBits(GPIOC,GPIO_Pin_13);
								GPIO_SetBits(GPIOB,GPIO_Pin_13);
								GPIO_SetBits(GPIOB,GPIO_Pin_15);
								GPIO_SetBits(GPIOC,GPIO_Pin_9);
	*/
	if(pin<=8)
		GPIO_SetBits  (GPIOB, GPIO_Pin_10);
	if(pin>8 && pin<=16)
		GPIO_SetBits  (GPIOB, GPIO_Pin_12);
	if(pin>16)
		GPIO_SetBits  (GPIOB, GPIO_Pin_14);
	
	switch(pin)
	{
		case 0: 
		case 9:
		case 18:
			GPIO_ResetBits(GPIOB,GPIO_Pin_5);	
		break;
		
		case 1: 
		case 10:
		case 19:
			GPIO_ResetBits(GPIOB,GPIO_Pin_6);	
		break;
		
		case 2: 
		case 11:
		case 20:
			GPIO_ResetBits(GPIOB,GPIO_Pin_7);	
		break;
		
		case 3: 
		case 12:
		case 21:
			GPIO_ResetBits(GPIOC,GPIO_Pin_11);	
		break;
		
		case 4: 
		case 13:
		case 22:
			GPIO_ResetBits(GPIOC,GPIO_Pin_12);	
		break;
		
		case 5: 
		case 14:
		case 23:
			GPIO_ResetBits(GPIOC,GPIO_Pin_13);
		break;
		
		case 6: 
		case 15:
		case 24:
			GPIO_ResetBits(GPIOB,GPIO_Pin_13);
		break;
		
		case 7: 
		case 16:
		case 25:
			GPIO_ResetBits(GPIOB,GPIO_Pin_15);
		break;
		
		case 8:
		case 17:
		case 26:			
			GPIO_ResetBits(GPIOC,GPIO_Pin_9);
		break;
	}
}
void setPinByNumber(int pin)
{
	/*
	GPIO_SetBits(GPIOB,GPIO_Pin_5);
	GPIO_SetBits(GPIOB,GPIO_Pin_6);
	GPIO_SetBits(GPIOB,GPIO_Pin_7);
	GPIO_SetBits(GPIOC,GPIO_Pin_11);
	GPIO_SetBits(GPIOC,GPIO_Pin_12);
	GPIO_SetBits(GPIOC,GPIO_Pin_13);
	GPIO_SetBits(GPIOB,GPIO_Pin_13);
	GPIO_SetBits(GPIOB,GPIO_Pin_15);
	GPIO_SetBits(GPIOC,GPIO_Pin_9);
	*/
	if(pin<=8)
		GPIO_ResetBits  (GPIOB, GPIO_Pin_10);
	if(pin>8 && pin<=16)
		GPIO_ResetBits  (GPIOB, GPIO_Pin_12);
	if(pin>16)
		GPIO_ResetBits  (GPIOB, GPIO_Pin_14);
	
	switch(pin)
	{
		case 0: 
		case 9:
		case 18:
			GPIO_SetBits(GPIOB,GPIO_Pin_5);	
		break;
		
		case 1: 
		case 10:
		case 19:
			GPIO_SetBits(GPIOB,GPIO_Pin_6);	
		break;
		
		case 2: 
		case 11:
		case 20:
			GPIO_SetBits(GPIOB,GPIO_Pin_7);	
		break;
		
		case 3: 
		case 12:
		case 21:
			GPIO_SetBits(GPIOC,GPIO_Pin_11);	
		break;
		
		case 4: 
		case 13:
		case 22:
			GPIO_SetBits(GPIOC,GPIO_Pin_12);	
		break;
		
		case 5: 
		case 14:
		case 23:
			GPIO_SetBits(GPIOC,GPIO_Pin_13);
		break;
		
		case 6: 
		case 15:
		case 24:
			GPIO_SetBits(GPIOB,GPIO_Pin_13);
		break;
		
		case 7: 
		case 16:
		case 25:
			GPIO_SetBits(GPIOB,GPIO_Pin_15);
		break;
		
		case 8:
		case 17:
		case 26:			
			GPIO_SetBits(GPIOC,GPIO_Pin_9);
		break;
	}
}


void delay_init(void) {

	 TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
   TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
   TIM_TimeBaseStructure.TIM_Prescaler = (SYS_CLK / DELAY_TIM_FREQUENCY) - 1;
   TIM_TimeBaseStructure.TIM_Period = 1;
   TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
   TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Down;
   TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
   //TIM_Cmd(TIM2, ENABLE);

}

void delay_us(uint16_t uSecs) {
	TIM_SetCounter(TIM2,uSecs);  
	TIM_Cmd(TIM2, ENABLE);
	while (TIM_GetCounter(TIM2) > 1) {}
	TIM_Cmd(TIM2,DISABLE);  


//	uint16_t start = TIM2->CNT;
   //while ((uint16_t)(TIM2->CNT - start) < uSecs);
}

void delay_ms(uint16_t mSecs) {
   uint16_t i;
   for (i = mSecs; i > 0; i--)
      delay_us(1000);
}

void USART1_Init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	
	// Enable clock of modules
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1, ENABLE);

  /* Configure the GPIO ports( USART1 Transmit and Receive Lines) */
  /* Configure the USART1_Tx as Alternate function Push-Pull */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Configure the USART1_Rx as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	// configure usart1
  USART_InitStructure.USART_BaudRate            = USART1_BAUDRATE;
  USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits            = USART_StopBits_1;
  USART_InitStructure.USART_Parity              = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &USART_InitStructure);

  /* Enable USART1 interrupt */
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  //USART_ITConfig(USART1,USART_IT_TXE,ENABLE);

  /* Enable the USART1 */
  USART_Cmd(USART1, ENABLE);
  
	// configure usart1 interrupt
	//NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0); 
	
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

}
