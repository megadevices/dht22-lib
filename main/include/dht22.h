#ifndef __DHT22_H__
#define __DHT22_H__

#include "stm32f10x.h"                  // Device header

/* Port and pin with DHT22 sensor*/
#define DHT22_GPIO_PORT            GPIOB
#define DHT22_GPIO_CLOCK           RCC_APB2Periph_GPIOB
#define DHT22_GPIO_PIN             GPIO_Pin_10

#define DHT22_ResetBit GPIO_ResetBits(DHT22_GPIO_PORT, DHT22_GPIO_PIN)
#define DHT22_SetBit GPIO_SetBits(DHT22_GPIO_PORT, DHT22_GPIO_PIN)
#define DHT22_Bit_1 (DHT22_GPIO_PORT->IDR & DHT22_GPIO_PIN)
#define DHT22_Bit_0 !(DHT22_GPIO_PORT->IDR & DHT22_GPIO_PIN)										  

/* DHT22_ReadData response codes */
#define DHT22_RCV_OK               0 // Return with no error
#define DHT22_RCV_NO_RESPONSE      1 // No response from sensor
#define DHT22_RCV_BAD_ACK1         2 // Bad first half length of ACK impulse
#define DHT22_RCV_BAD_ACK2         3 // Bad second half length of ACK impulse
#define DHT22_RCV_RCV_TIMEOUT      4 // It was timeout while receiving bits
#define DHT22_RCV_BAD_CRC			     5 // It was timeout while receiving bits


void DHT22_Init(void);
uint8_t DHT22_ReadData(void);
float DHT22getHumidity(void);
float DHT22getTemperature(void);

#endif // __DHT22_H__
